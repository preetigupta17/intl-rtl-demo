import React from 'react';
import { IntlProvider } from "react-intl";
import getTranslations from './translation/locales';
import AppWrapper from "./AppWrapper";

function App() {
  // get locale from url
  const locale = window.location.search.replace("?locale=","") || "en";
  console.log('####### locale : ', locale);
  
  const messages = getTranslations[locale];
  console.log('@@@@@@@ messages : ', messages);
  return (
    <IntlProvider
      locale={locale}
      key={locale} 
      messages={messages} 
      textComponent={React.Fragment}
      rtl>
      <AppWrapper />
    </IntlProvider>
  );
}

export default App;