import React from 'react';
import { injectIntl, defineMessages } from "react-intl";
import logo from './logo.svg';
import './App.css';

const messages = defineMessages({
    title: {
      id: 'app.title',
      defaultMessage: 'Welcome to React'
    },
    content1: {
      id: 'app.content1',
      defaultMessage: 'To get started, edit'
    },
    content2: {
      id: 'app.content2',
      defaultMessage: 'and save to reload.'
    },
    head: {
        id: 'app.head',
        defaultMessage: 'This is a demo project for RTL and react intl.',
    }
  })

function AppWrapper({ intl }) {

  return (
    <div className="App">
      <header className="App-header">
        <div className="languages">
          <a href="/?locale=ar">العربية</a>
          &nbsp;&nbsp;&nbsp;
          <a href="/?locale=en">English</a>
        </div>
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">{intl.formatMessage(messages.title)}</h1>
      
        <div style={{width: "500px", margin: "0 auto"}}>
          <p className="App-intro">
          {intl.formatMessage(messages.content1)} <code>src/App.js</code> {intl.formatMessage(messages.content2)}
          </p>
          <pre 
          style={{
            fontFamily: "inherit",
            textAlign: "left",
            whiteSpace: "break-spaces",
            fontSize: "16px" }}>
            {intl.formatMessage(messages.head)}
          </pre>
        </div>
        </header>
    </div>
  );
}

export default injectIntl(AppWrapper);
